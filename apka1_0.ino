#include <Servo.h>
#include<SoftwareSerial.h>
//definicje obiektów klasy serwo
Servo chwytak;
Servo arm1;
Servo arm2;
Servo arm3;
Servo basis;
Servo wheel_axis;
//definicja portów szeregowych
SoftwareSerial BT(3,2);// RX.3 -> TX.BT , TX.2 ->RX.BT   (pin arduino RX3 podlaczony do TX bluetootha)

String state; //definicja zmmiennej do przechowywania wysyłanych ciągów znaków

//zmienne do przechowywania pozycji
int chwytakppos;
int chwytakpos;
int arm1pos;
int arm1ppos;
int arm2pos;
int arm2ppos;
int arm3pos;
int arm3ppos;
int basisppos;
int basispos;
int wheel_axisppos;
int wheel_axispos;

int vel; //zmienna do przechowania prędkości
char command; //zmienna do przechowywania znaków

//flagi sygnalizacyjne
boolean gripperOn=false;
boolean arm1On=false;
boolean arm2On=false;
boolean arm3On=false;
boolean basisOn=false;
boolean axisOn=false;
boolean velocity=false;

int desired_position; //zmienna do zapisu pozycji

void setup() {
//ustawienie prędkości przeysłu
Serial.begin(9600); 
BT.begin(9600);

//połączenie zmiennej servo z pinem arduino
chwytak.attach(10);
arm1.attach(11);
arm2.attach(12);
arm3.attach(13);
basis.attach(8);
wheel_axis.attach(9);

//przypisanie pozycji wyjsciowych do zmiennych
chwytakppos=60; 
arm1ppos=90;
arm2ppos=90;
arm3ppos=80;
basisppos=90;
wheel_axisppos=40;

//ustawienie serwomechanizmów w pozycjach wejściowych
chwytak.write(chwytakppos);
arm1.write(arm1ppos);
arm2.write(arm2ppos);
arm3.write(arm3ppos);
basis.write(basisppos);
wheel_axis.write(wheel_axisppos);

//podłączenie mostka h i silnika
pinMode(6, OUTPUT); //DC Motor
pinMode(4, OUTPUT); //Mostek h
pinMode(5, OUTPUT); //Mostek h

}

void loop() {
  // put your main code here, to run repeatedly:
  if(Serial.available()>0) //warunek potrzebny do wyświetalnia informacji w monitorze portu szeregowego
  {
  }
  if (BT.available() > 0) //sprawdź czy jest coś zapisane w buforze portu szeregowego
    {state = "";} // zapis wysyłanych danych do stringa
    while(BT.available() > 0) //pętla będzie wykonywana do póki są dane w buforze
    {
      command = ((byte)BT.read()); //zapisz odczytane bajty do zmiennej command
      
      if(command == ':') //sprawdź czy pojawil się znak
      {
        break; //przerwij pętle
      }
      
      else
      {
        state += command; //dodaj bajty do zmiennej state
      }
      
      delay(1); //opóźnienie
    }

    //////////////////////////////////CHWYTAK/////////////////////////////
    if(state=="GO") //jeżeli zostaną wysłane znaki "GO"
    {
      gripperOn=true;//aktywuj flagę chwytaka
      arm1On=false;//dezaktywuj resztę flag
      arm2On=false;
      arm3On=false;
      basisOn=false;
      axisOn=false;
      Serial.println(state); //wypisz znaki w monitorze
      delay(10); //odczekaj 10 ms
      //state = "";
      }

 
    if ((gripperOn==true)&&(state.toInt()>=0)&&(state.toInt()<=255)&&(desired_position!=state.toInt()))
    //wykonaj poniższy kod w momencie kiedy aktywowana została flaga, wysyłane liczyby zwierają się w przedziale
    //<0,255> i wysyłana pozycja nie jest pozycją pożądaną
    {
        chwytakpos=state.toInt(); //zapisz liczby całkowite do zmiennej przechowującą pozycję chywata

        if (chwytakppos > chwytakpos) { //jeżeli poprzednia pozycja jest większa od aktualnej
        for ( int j = chwytakppos; j >= chwytakpos; j--) {   // porusz serwem w dół
          chwytak.write(j);//wykonaj ruch
          delay(10);    // określa prędkość z jaką ma poruszać się serwo
        }
      }
      // jeżeli poprzednia pozycja jest mniejsza od akutalnej
      if (chwytakppos < chwytakpos) {
        for ( int j = chwytakppos; j <= chwytakpos; j++) {   // porusz serwem do góry
          chwytak.write(j);
          delay(10);
        }
      }
      chwytakppos = chwytakpos;   // zapisz aktualna pozycję jako poprzednią
      //state = ""; //wyczyść zmienną state
    }
        
        
       
    //////////////////////////ARM 1/////////////////////////////////
     if(state=="AO")
    {
     
      arm1On=true;
      gripperOn=false;
      arm2On=false;
      arm3On=false;
      basisOn=false;
      axisOn=false;
      arm1.write(arm1ppos);
      Serial.println(state);
      delay(10);
      //state = "";
      }

    if ((arm1On==true)&&(state.toInt()>=0)&&(state.toInt()<=255)&&(desired_position!=state.toInt()))
    {
        arm1pos=state.toInt();

        if (arm1ppos > arm1pos) {
        for ( int l = arm1ppos; l >= arm1pos; l--) {   // Run servo down
          arm1.write(l);
          delay(10);    // defines the speed at which the servo rotates
        }
      }
      // If previous position is smaller then current position
      if (arm1ppos < arm1pos) {
        for ( int l = arm1ppos; l <= arm1pos; l++) {   // Run servo up
          arm1.write(l);
          delay(10);
        }
      }
      arm1ppos = arm1pos; // set current position as previous position
      //state = "";
     }

/////////////////////////////////////////ARM 2///////////////////////////////////

     if(state=="BO")
    {
     
      arm2On=true;
      gripperOn=false;
      arm1On=false;
      arm3On=false;
      basisOn=false;
      axisOn=false;
      arm2.write(arm2ppos);
      Serial.println(state);
      delay(10);
      //state = "";
      }

 
    if ((arm2On==true)&&(state.toInt()>=0)&&(state.toInt()<=255)&&(desired_position!=state.toInt()))
    {
        arm2pos=state.toInt();

        if (arm2ppos > arm2pos) {
        for ( int k = arm2ppos; k >= arm2pos; k--) {   // Run servo down
          arm2.write(k);
          delay(10);    // defines the speed at which the servo rotates
        }
      }
      // If previous position is smaller then current position
      if (arm2ppos < arm2pos) {
        for ( int k = arm2ppos; k <= arm2pos; k++) {   // Run servo up
          arm2.write(k);
          delay(15);
        }
      }
      arm2ppos = arm2pos;   // set current position as previous position
      //state = "";
     } 

////////////////////////////////////////////////ARM 3//////////////////////////////////////////
      if(state=="CO")
    {
     
      arm3On=true;
      gripperOn=false;
      arm1On=false;
      arm2On=false;
      basisOn=false;
      axisOn=false;
      arm3.write(arm3ppos);
      Serial.println(state);
      delay(10);
      //state = "";
      }

 
 
    if ((arm3On==true)&&(state.toInt()>=0)&&(state.toInt()<=255)&&(desired_position!=state.toInt()))
    {
        arm3pos=state.toInt();

        if (arm3ppos > arm3pos) {
        for ( int p = arm3ppos; p >= arm3pos; p--) {   // Run servo down
          arm3.write(p);
          delay(20);    // defines the speed at which the servo rotates
        }
      }
      // If previous position is smaller then current position
      if (arm3ppos < arm3pos) {
        for ( int p = arm3ppos; p <= arm3pos; p++) {   // Run servo up
          arm3.write(p);
          delay(20);
        }
      }
      arm3ppos = arm3pos;   // set current position as previous position
      //state = "";
     }

//////////////////////////////////////////////////////////BASIS//////////////////////////////////////////////
       if(state=="DO")
    {
     
      basisOn=true;
      gripperOn=false;
      arm1On=false;
      arm2On=false;
      arm3On=false;
      axisOn=false;
      basis.write(basisppos);
      Serial.println(state);
      delay(10);
     // state = "";
      }

 
    if ((basisOn==true)&&(state.toInt()>=0)&&(state.toInt()<=255)&&(desired_position!=state.toInt()))
    {
        basispos=state.toInt();

        if (basisppos > basispos) {
        for ( int u = basisppos; u >= basispos; u--) {   // Run servo down
          basis.write(u);
          delay(10);    // defines the speed at which the servo rotates
        }
      }
      // If previous position is smaller then current position
      if (basisppos < basispos) {
        for ( int u = basisppos; u <= basispos; u++) {   // Run servo up
          basis.write(u);
          delay(10);
        }
      }
      basisppos = basispos;   // set current position as previous position
      //state = "";
     }  
//////////////////////////////////////////////////PLATFORMA//////////////////////////////////////////////////////////////
    if(state=="ST")// jeżeli wystąpią te znaki
    {
     axisOn=true; //aktywuj flagę sterowania osią
     gripperOn=false;//dezaktywuj pozostałe flagi
      arm1On=false;
      arm2On=false;
      arm3On=false;
      basisOn=false;
     wheel_axis.write(wheel_axisppos);
     Serial.println(state);
     delay(10); 
     //state = "";
    }
    if ((axisOn==true)&&(state.toInt()>=0)&&(state.toInt()<=255)&&(desired_position!=state.toInt()))
    //wykonaj poniższy kod w momencie kiedy aktywowana została flaga, wysyłane liczyby zawierają się w przedziale
    //<0,255> i wysyłana pozycja nie jest pozycją pożądaną
    {
        wheel_axispos=state.toInt(); //zapisz wartość do pozycji aktualnej

        if (wheel_axisppos > wheel_axispos) { //wykonaj jeżeli poprzednia pozycja jest większa od aktualnej
        for ( int g = wheel_axisppos; g >= wheel_axispos; g--) {   // porusz serwem w dół
          wheel_axis.write(g);
          delay(10);    
        }
      }
      // jeżeli poprzednia pozycja jest mniejsza od aktualnej
      if (wheel_axisppos < wheel_axispos) {
        for ( int g = wheel_axisppos; g <= wheel_axispos; g++) {   // porusz serwem w dół
          wheel_axis.write(g);
          delay(10);
        }
      }
      wheel_axisppos = wheel_axispos; // zapisz aktualną pozycję jako poprzednią
      //state = "";//wyczyść state
     }

    if(state=="SLO")
    {
      vel=150;
      }
     if(state=="FST")
     {
      vel=230;
      } 
    if(state=="RUN")//jeżeli odczytano podane znaki
    {
      digitalWrite(4, LOW); //ustaw w stan niski pin 4
      digitalWrite(5, HIGH); //ustaw w stan wysoki pin 5
      analogWrite(6, vel); //uruchom silniki podłączone do pinu 6 z prędkością zapisaną w zmiennej
      //state = ""; 
      }
    if(state=="BACK")
    {
      digitalWrite(4, HIGH);
      digitalWrite(5, LOW);
      analogWrite(6, vel);
     // state = "";
      }
    if(state=="RST")
    {
      digitalWrite(4, LOW);//ustaw w stan niski piny mostka
      digitalWrite(5, LOW);
      analogWrite(6, 0);//wyłącz silnik
      wheel_axis.write(40);//ustaw serwo w pozycji neutralnej
      chwytak.write(60);
      arm1.write(90);
      arm2.write(90);
      arm3.write(80);
      basis.write(90);
     // state = "";
      }
     if(state=="STO")
     {
      digitalWrite(4, LOW);//ustaw w stan niski pin 4
      digitalWrite(5, LOW);//ustaw w stan niski pin 5
      analogWrite(6, 0); //zatrzymaj silnik
      //state = "";
      }
    
}
